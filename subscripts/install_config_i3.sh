
#install i3-gaps and i3blocks
#install xbacklight to control backlight
#install feh to change backgrounds
#install conpron to make the terminal transparent
#install rofi for the menu
sudo add-apt-repository -y ppa:regolith-linux/release
sudo apt update -y
sudo apt install -y i3-gaps
sudo apt install -y i3blocks xbacklight feh compton rofi maim xdotool #imagestick

#install acpilight to control backlight (to fix xbacklight)
git clone https://gitlab.com/wavexx/acpilight
cd acpilight
sudo make install
#add david to the video group - needs logout
sudo usermod -a -G video david

# Clone utilities for i3 blocks
git clone https://github.com/vivien/i3blocks-contrib ~/.config/i3blocks
