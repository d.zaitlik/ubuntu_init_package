#!/bin/bash
echo "*********************************"
echo "         INSTALL ZSHELL          "
echo "*********************************"

sudo apt install -y zsh
sudo apt-get install -y powerline fonts-powerline

echo "*********************************"
echo "        INSTALLED ZSHELL         "
echo "*********************************"

echo "*********************************"
echo "       INSTALL OH-MY-ZSH         "
echo "*********************************"

rm -rf ~/.oh-my-zsh
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
rm ~/.zshrc
cp zshrc.zsh-template ~/.zshrc
source ~/.zshrc
#zsh

#echo "Changing shell requires password"
chsh -s $(which zsh)

zsh

echo "*********************************"
echo "      INSTALLED OH-MY-ZSH        "
echo "*********************************"

exit 0
