#!/bin/bash

echo "*********************************"
echo " INSTALLING TOMAS'S LINUX SETUP  "
echo "*********************************"

cd /tmp
echo "mkdir -p ~/git
cd ~/git
sudo apt-get -y install git
git clone https://github.com/klaxalk/linux-setup.git
cd linux-setup
./install.sh" > run.sh && source run.sh

sudo echo "/usr/bin/zsh" >> /etc/shells
chsh -s /usr/bin/zsh
