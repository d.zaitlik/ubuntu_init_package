#!/bin/bash

function show_usage (){
	echo "Usage: $0 [options [parameters]]"
	echo ""
	echo "-h|--help, Prints help"
	echo "-d|--dir, Selects where to clone repositories."

return 0
}
dir_to_clone=""

# Parse arguents if any
case "$1" in
	-h|--help)
	show_usage
	exit 0
	;;
	-d|--dir)
	shift
	dir_to_clone="$1"
	echo "$1"
	;;
	*)
	show_usage
	exit 0
esac

echo "*********************************"
echo "      INSTALLING GITLABBER       "
echo "*********************************"

pip install gitlabber

echo "*********************************"
echo "      INSTALLED GITLABBER        "
echo "*********************************"
echo ""
echo "*********************************"
echo "   CLONING HDIOT REPOSITORIES    "
echo "*********************************"

gitlabber -t glpat-6yJTuSw6BArsnpU_q4qT -u https://www.gitlab.com/ -i '/HDIoT**' $1

echo "*********************************"
echo "    CLONED HDIOT REPOSITORIES    "
echo "*********************************"

exit 0
