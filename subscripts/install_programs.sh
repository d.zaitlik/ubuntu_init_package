#!/bin/bash

echo "*********************************"
echo "      INSTALLING PROGRAMS        "
echo "*********************************"

echo "*********************************"
echo "      INSTALLING SMARTSCOPE      "
echo "*********************************"

mkdir tmp
cd tmp
wget -O smartscope.deb --convert-links https://www.lab-nation.com/package/smartscope/linux/latest/get
sudo dpkg -i smartscope.deb
cd ..
rm -rf tmp

echo "*********************************"
echo "       INSTALLING SIGNAL         "
echo "*********************************"

# NOTE: These instructions only work for 64 bit Debian-based
# Linux distributions such as Ubuntu, Mint etc.
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update && sudo apt install -y signal-desktop
rm signal-desktop-keyring.gpg

echo "*********************************"
echo "INSTALLING TUXEDO CONTROL CENTER "
echo "*********************************"

sudo cp tuxedocomputers.list /etc/apt/sources.list.d/tuxedocomputers.list
wget -O - http://deb.tuxedocomputers.com/0x54840598.pub.asc | sudo apt-key add -
sudo apt-key adv --fingerprint 54840598
sudo apt update && sudo apt install tuxedo-control-center

echo "*********************************"
echo "       INSTALLED PROGRAMS        "
echo "*********************************"

exit 0
