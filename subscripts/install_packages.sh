#!/bin/bash

echo "*********************************"
echo "        PROGRAM INSTALLER        "
echo "*********************************"

echo "---------------------------------"
echo "        UPDATING PACKAGES        "
echo "---------------------------------"
sudo apt update -y

echo "---------------------------------"
echo "        UPGRADING PACKAGES       "
echo "---------------------------------"
sudo apt upgrade -y

echo "---------------------------------"
echo "INSTALLING YOUR FAVORITE PROGRAMS"
echo "---------------------------------"

programs=( git freecad blender inkscape gimp vlc vim cutecom kcalc python python3 python3-pip network-manager cmake terminator arandr bluez-tools network-manager-gnome )

for program in "${programs[@]}"
do
  echo "Installing ${program}"
  sudo apt install -y $program
done
sudo apt autoremove -y
echo "---------------------------------"
echo "    INSTALLING PROGRAMS DONE     "
echo "---------------------------------"

exit 0
