#!/bin/bash

echo "*********************************"
echo "      INSTALL KICAD PLUGINS      "
echo "*********************************"

echo "---------------------------------"
echo "  CREATE KICAD PLUGIN DIRECTORY  "
echo "---------------------------------"
mkdir -p ~/.kicad/scripting/plugins
cd ~/.kicad/scripting/plugins

echo "---------------------------------"
echo " INSTALL INTERRACTIVE BOM PLUGIN "
echo "---------------------------------"
git clone https://github.com/openscopeproject/InteractiveHtmlBom.git

echo "---------------------------------"
echo "     INSTALL RF TOOLS PLUGIN     "
echo "---------------------------------"
git clone https://github.com/easyw/RF-tools-KiCAD.git

echo "---------------------------------"
echo "  INSTALL ACTION SCRIPTS PLUGIN  "
echo "---------------------------------"
git clone https://github.com/jsreynaud/kicad-action-scripts.git

echo "*********************************"
echo "    INSTALLED KICAD PLUGINS      "
echo "*********************************"

exit 0;
