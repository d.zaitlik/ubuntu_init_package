#!/bin/bash
echo "*********************************"
echo "     INITIALIZING ENVIROMENT     "
echo "*********************************"
./subscripts/install_packages.sh
./subscripts/install_kicad_plugins.sh
./subscripts/install_programs.sh
sudo apt --fix-broken install
mkdir -p ~/Personal/
sudo ./subscripts/clone_hdiot_repos.sh -d ~/Personal/
#./subscripts/install_zsh.sh
#sudo apt --fix-broken install
echo "*********************************"
echo "     INITIALIZED ENVIROMENT      "
echo "*********************************"

exit 0

